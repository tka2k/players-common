Camera booth helper

## Features

- Add layer on a picked image

## Setup

In `pubspec.yaml`:

```yaml
  tka2k_booth:
    git:
      url: https://bitbucket.org/tka2k/players-common.git
      path: packages/tka2k_booth
      ref: dart2_3
      version: '>=0.1.0'
```
## Example

See [Demo app](../../example/app_pick_crop_image_demo)

## Usage

Needed import

```dart
import 'package:tka2k_booth/tka2k_booth.dart';
```

Simple example with an overlay asset exported as jpg

The result is available in the imageData returned
* bytes
* width
* height
* encoding

See `pickCropImage` for information.

```dart
// Pick and crop an image from the camera and apply an overlay
var imageData = await pickBoothImage(context,
  // Pick crop options (here square
  pickCropOptions: PickCropImageOptions(
    width: 1024,
    height: 1024,
    source: const PickCropImageSourceCamera(
      preferredCameraDevice: SourceCameraDevice.front),
    // Encode as JPEG
    encoding: ImageEncodingJpg(quality: 75)),
  // Booth overlay, stretched on top of the image - here from an asset
  options: PickBoothImageOptions(
    overlay: ImageSourceAsset(name: 'assets/img/overlay_theater.png')));

```
