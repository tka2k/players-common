// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:tekartik_app_image/app_image.dart';
import 'package:tekartik_app_image_web/src/image_composer/image_composer.dart';
import 'package:tekartik_app_pick_crop_image_flutter/src/pick_crop_image.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

/// Pick crop image options.
class PickBoothImageOptions {
  /// Overlay
  final ImageSource? overlay;

  PickBoothImageOptions({this.overlay});

  Map toMap() => {if (overlay != null) 'overlay': overlay};
  @override
  String toString() => toMap().toString();
}

/// Pick an image and apply an overlay
///
/// width and height must be specified in the cropOptions
Future<ImageData?> pickBoothImage(BuildContext context,
    {required PickCropImageOptions pickCropOptions,
    PickBoothImageOptions? options}) async {
  assert(pickCropOptions.width != null, 'Width must be specified');
  assert(pickCropOptions.height != null, 'Width must be specified');
  var result = await pickCropImageInternal(context, options: pickCropOptions,
      callback: (param) async {
    var data = ImageComposerData(
        width: param.options.width!,
        height: param.options.height!,
        encoding: pickCropOptions.encoding,
        layers: [
          ImageLayerData(
              source: param.imageSource, sourceCropRect: param.cropRect),
          if (options?.overlay != null)
            ImageLayerData(source: options!.overlay!),
        ]);
    var imageData = await composeImage(data);
    return imageData;
  });
  return result;
}
