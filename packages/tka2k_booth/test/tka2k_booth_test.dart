import 'package:flutter_test/flutter_test.dart';
import 'package:tka2k_booth/tka2k_booth.dart';

void main() {
  test('adds one to input values', () {
    var options = PickBoothImageOptions();
    expect(options.toMap(), {});
    var image = ImageSourceAsset(name: 'test');
    expect(image.toString(), 'ImageSourceAsset(test)');
    options = PickBoothImageOptions(overlay: image);
    expect(options.toMap(), {'overlay': image});
  });
}
