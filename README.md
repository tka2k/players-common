Tekartik/A2K helpers

### Content

| Package | Description |
| --- | --- |
| [tka2k_booth](packages/tka2k_booth) | Camera Booth helpers |

| Example | Description |
| ---- | --------- |
| [Example app](example/tka2k_booth_example) | Demo app ([Online web demo](http://a2kbooth-demo.web.app/)) |

Project sponsored by A2K Technologies.