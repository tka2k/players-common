# tka2k_booth_example

Booth demo ([Online web demo](https://a2kbooth-demo.web.app))

Work on Desktop, Mobile and Web.

## Setup

Project files are not checked so this must be performed once

```shell
flutter create .
flutter run
```