// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:tka2k_booth/tka2k_booth.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'A2K booth demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _result = ValueNotifier<ImageData?>(null);

  Future<void> _pickBooth() async {
    var result = await pickBoothImage(context,
        pickCropOptions: PickCropImageOptions(
          width: 1024,
          height: 1024,
          source: const PickCropImageSourceCamera(
              preferredCameraDevice: SourceCameraDevice.front),
        ),
        options: PickBoothImageOptions(
            overlay: ImageSourceAsset(name: 'assets/img/overlay_theater.png')));
    print('result: $result');
    if (result != null) {
      _result.value = result;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('A2K booth demo'),
      ),
      body: SingleChildScrollView(
          child: Center(
              child: SizedBox(
        width: 640,
        child: Column(children: [
          const SizedBox(
            height: 16,
          ),
          ImageDataWidget(result: _result),
          const SizedBox(
            height: 64,
          )
        ]),
      ))),
      floatingActionButton: FloatingActionButton(
        onPressed: _pickBooth,
        tooltip: 'Pick booth',
        child: const Icon(Icons.camera),
      ),
    );
  }
}

class ImageDataWidget extends StatelessWidget {
  const ImageDataWidget({
    Key? key,
    required ValueNotifier<ImageData?> result,
  })  : _result = result,
        super(key: key);

  final ValueNotifier<ImageData?> _result;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<ImageData?>(
        valueListenable: _result,
        builder: (context, result, _) {
          if (result != null) {
            return Column(children: [
              const SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Row(
                  children: [
                    Text(
                        '${result.width}x${result.height}, ${result.bytes.length} bytes, ${result.encoding.mimeType}'),
                    Expanded(child: Container()),
                    ElevatedButton(
                        onPressed: () {
                          saveImageFile(
                              bytes: result.bytes,
                              mimeType: result.encoding.mimeType,
                              filename: 'image${result.encoding.extension}');
                        },
                        child: const Text('Save image')),
                  ],
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: AspectRatio(
                      aspectRatio: 1,
                      child: AspectRatio(
                          aspectRatio: result.width / result.height,
                          child: Image.memory(
                            result.bytes,
                            fit: BoxFit.contain,
                          ))))
            ]);
          }
          return Container();
        });
  }
}
